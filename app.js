console.log( "--------------------" );
console.log( "Geocode Cache Server" );
console.log( "--------------------" );

/* ---- SETUP EXPRESS ---- */
console.log( "Setting up Express" );
var express = require( "express" );
var bodyParser = require( "body-parser" );

console.log( "Setting up Application" );
var app = express();
app.use( bodyParser.json() );

/* ---- SETUP POUCHDB ---- */ 
console.log( "Setting up PouchDB" );
var PouchDB = require( 'pouchdb' );
PouchDB.debug.enable('*');

// PouchDB.destroy( "geocode" );
// return;


var db = new PouchDB( 'geocode' ); 
num_requests = 0;


/* ---- SETUP SYNC WITH COUCH ---- */
console.log( "Setting up Sync with CouchDB" );
var sync = PouchDB.sync( "geocode", "http://localhost:5984/geocode", {live: true} );

/* ---- SETUP GOOGLEMAPS ---- */
console.log( "Setting up GoogleMaps API" );
var gm = require( "googlemaps" );

/* ---- SETUP NOKIA APPI ---- */
console.log( "Setting up Nokia API" );
var nokia = require('nokiaplaces');

nokia.places.settings.setAppContext({
            app_id: 'HfguEArjaP8S47j2SCzM',
            app_code: 'CSFSNsRlI297Qo1Q4RCgZQ'
          });


/* ---- OUTPUT POUCHDB INFO ---- */
db.info().then(function (info) {
  console.log( "Pouch DB Info: " );
  console.log(info);
})

app.get( "/testnokia", function( req, res ) {   
  
  nokia.places.search.manager.findPlaces({
            searchTerm: 'pizza',
            searchCenter: {
              latitude: 52.516274,
              longitude: 13.377678
            },
            onComplete: function(data, status){
               console.log(data);
               res.send( data );
            }
          });

} );


/* ---- GEOCODE REQUEST ---- */ 
app.post( "/geocode", function( req, res ) {
  if ( req.body.address ) {
    var address = req.body.address;

    var search_for_address = function() {
      console.log( "Not in cache. Searching Provider for " + address );

      var gm_callback = function( err, result ) {
        console.log( "gm_callback: start" );
        
        if ( err == null ) {
          if ( result ) {
            if ( result.status == "OK" ) {
              console.log( "Provider Found address: " + address );
              num_requests++;
              console.log( "Provider requests this session: " + num_requests );
              var place = {
                _id                 : address,
                longitude           : result.results[0].geometry.location.lng,
                latitude            : result.results[0].geometry.location.lat,
                formatted_address   : result.results[0].formatted_address                
              };
              console.log( "Adding it to PouchDB" );
              db.put( place ).then( function( response ) { 
                db.get( address ).then( function( data )  { res.send( data ) } );
              } );
            }
          }
        } else {
          throw err;
        }
      };

      console.log( "Calling Geocode" );
      gm.geocode( address, gm_callback, false );
      
    }

    console.log( "Searching for: " + req.body.address );

    db.get( address ).catch( 
      function( err ) {
        if ( err.status === 404 ) {
          search_for_address();
          console.log( "After search_for_address");
        } else {
          throw err;
        }
      }
    ).then( 
      function( doc ) {
        if ( typeof doc !== "undefined" && doc ) {
          console.log( "Got the cache!" );          
          res.send( doc );          
        }
      }
    ).catch( 
      function( err ) {
        console.log( "Error getting address" );
        console.log( err );
        res.send( err );
      }
    );
  } else {
    res.send( "{}" );
  }
} );

/* ---- LIST ALL GEOCODES ---- */
app.get( "/geocode", function( req, res ) {
  db.allDocs( { include_docs: true } ).then( function( doc ) {
    res.send( doc );
  })
} );

/* ---- INDEX ---- */
app.get( "/", function( req, res ) {
  res.send( "Geocode Cache Server v 1.0" );
});


/* ---- SETUP SERVER ---- */
console.log( "Setting up Server" );
var server = app.listen( 8080, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log( "Geocode Cache server listening http://%s:%s", host, port );
})
